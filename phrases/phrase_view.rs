use crate::Cmd;
use super::phrase::Phrase;

pub struct PhraseView {}

impl PhraseView {

    pub fn get_phrase () -> (String, String) {

        let de: String = Self::request_de();
        let ru: String = Self::request_ru();
        
        return (de, ru);
    }

    pub fn request_de () -> String {

        return Cmd::get("de:");
    }

    pub fn request_ru () -> String {

        return Cmd::get("ru:");
    }

    pub fn request_phrase () -> String {

        return Cmd::get("Phrase: ");
    }

    pub fn show_phrase (phrase: &Phrase) {

        println!("|de: {}\n|ru: {}", phrase.de, phrase.ru);
    }

    pub fn show_list (list: Vec<&String>) {

        for phrase in list.iter() {
            println!("|{}", phrase);
        }
    }
}
