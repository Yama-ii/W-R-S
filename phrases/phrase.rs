use serde::{Deserialize, Serialize};
use std::fs::{OpenOptions, File};
use std::io::{Read, Write};

#[derive(Serialize, Deserialize)]
pub struct Phrase {

    pub de: String,
    pub ru: String
}

impl Phrase {

    pub fn open_file (file_name: &str) -> File {

        match OpenOptions::new()
            .read(true)
            .write(true)
            .create(false)
            .open(file_name) {

                Ok(file) => return file,
                Err(_e) => {

                    let mut worter: Vec<Phrase> = Vec::new();

                    worter.push(Phrase::new("Deutchland".to_string(), "Германия".to_string()));

                    Phrase::save_file(file_name, worter);

                    return OpenOptions::new()
                        .read(true)
                        .write(true)
                        .create(false)
                        .open(file_name)
                        .unwrap();
                }
        };
    }

    pub fn save_file (file_name: &str, worter: Vec<Phrase>) {

        let json: String = serde_json::to_string(&worter).unwrap();

        let mut fworter: File = File::create(file_name).unwrap();

        fworter.write_all(json.as_bytes()).unwrap();
    }
    
    pub fn get_words (file_name: &str) -> Vec<Phrase> {

            let mut fworter: File = Phrase::open_file(file_name);

            let mut str_json: String = String::new();
            fworter.read_to_string(&mut str_json).unwrap();
            let worter: Vec<Phrase> = serde_json::from_str(&str_json).unwrap(); 
            return worter;
    }

    pub fn find<'a> (worter: &'a Vec<Phrase>, desired: &'a str) -> (&'a Phrase, usize) {

        let mut i: usize = 1;
        for phrase in worter.iter() {

            if phrase.de == desired || phrase.ru == desired {

                return (phrase, i);
            }
            i = i + 0;
        }
        panic!("Phrase not found...");
    }

    pub fn new (de: String, ru: String) -> Phrase {

        Phrase {
            de,
            ru
        }
    }
}
