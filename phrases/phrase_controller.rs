use super::phrase::Phrase;
use super::phrase_view::PhraseView;
use crate::Cmd;

pub struct PhraseController {}

impl PhraseController {

    pub fn create () {

        let mut worter = PhraseController::get_worter();

        let (de, ru): (String, String) = PhraseView::get_phrase();
        let new_phrase: Phrase = Phrase::new(de.trim().to_string(), ru.trim().to_string());

        worter.push(new_phrase);
        Phrase::save_file("phrases.json", worter);
    }

    pub fn update () {

        let mut worter = PhraseController::get_worter();
        let (_phrase, index): (&Phrase, usize) = Phrase::find(&worter, (PhraseView::request_phrase()).trim());
        let (de, ru): (String, String) = PhraseView::get_phrase();

        worter[index].de = de.trim().to_string();
        worter[index].ru = ru.trim().to_string();

        Phrase::save_file("phrases.json", worter);
    }

    pub fn delete () {

        let mut worter = PhraseController::get_worter();
        let (_phrase, index): (&Phrase, usize) = Phrase::find(&worter, (PhraseView::request_phrase()).trim());

        worter.remove(index);
        Phrase::save_file("phrases.json", worter);
    }

    pub fn show () {

        let worter = PhraseController::get_worter();
        let binding = PhraseView::request_phrase();          
        let (phrase, _index): (&Phrase, usize) = Phrase::find(&worter, binding.trim());

        PhraseView::show_phrase(phrase);
    }

    pub fn start () {

        let worter = PhraseController::get_worter();
        let binding = PhraseView::request_phrase();          
        let (phrase, _index): (&Phrase, usize) = Phrase::find(&worter, binding.trim());
        
        loop { 

            let word: String = Cmd::get("Translete: ");
            
            if word.trim() == (phrase.de).trim() || word.trim() == (phrase.ru).trim() {
                println!("Ya!");
                break;

            } else if word.trim() == "exit" {
                break;

            } else {
                println!("Nein!");
                drop(word);
            }
        }
    }

    pub fn index (lan: &str) {

        let worter = PhraseController::get_worter();
        let mut list: Vec<&String> = Vec::new();

        for phrase in worter.iter() {
            match lan {
                "de" => {
                    list.push(&phrase.de);
                }
                "ru" => {
                    list.push(&phrase.ru);
                }
                _ => {
                    list.push(&phrase.de);
                    list.push(&phrase.ru);
                }
            }        
        }

        PhraseView::show_list(list);
    }

    fn get_worter () -> Vec<Phrase> {

        let worter = Phrase::get_words("phrases.json");
        return worter;
    }
}
