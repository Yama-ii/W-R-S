use std::io::stdin;

pub struct Cmd {}

impl Cmd {

    pub fn get (message: &str) -> String {

        println!("\n{}", message);

        let mut response: String = String::new();
        stdin().read_line(&mut response).unwrap();

        return response;
    }
}
