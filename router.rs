use std::process::exit;
use crate::PhraseController;

pub struct Router {}

impl Router {
    
    pub fn request (request: String) {

        match request.trim() {

            "add"     => PhraseController::create(),
            "edit"    => PhraseController::update(),
            "delete"  => PhraseController::delete(),
            "show"    => PhraseController::show(),
            "list ru" => PhraseController::index("ru"),
            "list de" => PhraseController::index("de"),
            "list"    => PhraseController::index(""),
            "start"   => PhraseController::start(),

            "exit"    => exit(0),
            _othe     => println!("Command not faund...")
        }
    }
}
